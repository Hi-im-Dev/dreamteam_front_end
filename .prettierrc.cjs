module.exports = {
  arrowParens: 'always',
  bracketSameLine: true,
  bracketSpacing: true,
  singleQuote: true,
  jsxSingleQuote: false,
  trailingComma: 'all',
  tabWidth: 2,
  printWidth: 140,
  useTabs: false,
  endOfLine: 'lf',
};
