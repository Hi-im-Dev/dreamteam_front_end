import React from 'react';
import './ModalForm.css';
import { t } from 'i18next';

type Props = {
  handleSubmit: (event: React.FormEvent<HTMLFormElement>) => void;
  children: React.ReactNode;
  handleClose: () => void;
};

function ModalForm({ handleSubmit, children, handleClose }: Props) {
  return (
    <div className="server-form-modal">
      <div className="server-form-modal-content">
        <form onSubmit={(e) => handleSubmit(e)}>
          <div>{children}</div>
          <div>
            <button type="submit">{t('create')}</button>
            <button type="button" className="cancel" onClick={handleClose}>
              {t('cancel')}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default ModalForm;
