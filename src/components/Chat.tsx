import React, { useContext, useEffect, useState } from 'react';
import { UserContext } from '../context/UserContext';
import axios from '../services/Axios';
import './Chat.css';

type Message = {
  channelId: number,
  username: string,
  content: string,
  datetime: string,
}

function Chat() {
  const [input, setInput] = useState<string>('');
  const [messages, setMessages] = useState<Message[]>([]);

  const { user, socket, channelId } = useContext(UserContext)

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInput(event.target.value);
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      if (input) {
        socket?.send(JSON.stringify({ channelId: channelId, content: input }));
        setInput('');
      }
    }
  };

  useEffect(() => {
    if (!channelId) return
    setMessages([])
    axios.get<Message[]>(`/messages/${channelId}`)
    .then(({data}) => {
      if (!data || !data.length) return
      setMessages([...data.reverse()])
    })
    .catch((e) => console.log(e))
  }, [channelId])

  useEffect(() => {
    socket?.addEventListener('message', (event) => {
      const data = JSON.parse(event.data) as Message;
      console.log(data)
      setMessages(old => [data, ...old])
    })
  }, [socket])

  if (!socket) return <></>

  return (
    <div className="chat-container">
      <div className="chat-messages">
        {messages.map((message, index) => {
          return (
          <div key={index} className="message">
            <div className="message-title">
              <span className="author">{message.username}</span>
              <span className="datetime">
                {message.datetime}
              </span>
            </div>
            <span className="message-text">{message.content}</span>
          </div>
        )})}
        <div ref={(el) => el?.scrollIntoView({ behavior: 'smooth' })} />
      </div>
      <input
        type="text"
        value={input}
        placeholder="Envoyer un message..."
        onChange={handleChange}
        className="chat-input"
        onKeyDown={handleKeyDown}
      />
    </div>
  );
}

export default Chat;
