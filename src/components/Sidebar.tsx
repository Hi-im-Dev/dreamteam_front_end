import React, { useState, useContext, useEffect } from 'react';
import toast from 'react-hot-toast';
import { UserContext } from '../context/UserContext';
import SidebarItem from './Sidebartem';
import './Sidebar.css'; // Import CSS file
import axios from '../services/Axios';
import ModalForm from './ModalForm';
import {
  Channel, getChannel, getServer, Server,
} from '../services/Servers';
import { connectWebSocket } from '../services/Socket';
import { t } from 'i18next';

export function ServerSidebar() {
  const { setUser, serverId, setServerId } = useContext(UserContext);
  const [servers, setServers] = useState<Server[]>([]);
  const [isCreatingServer, setIsCreatingServer] = useState(false);
  const [serverName, setServerName] = useState('');

  useEffect(() => {
    const handleFetchServer = () => getServer().then((servers) => {
      setServers(servers);
    });
    handleFetchServer();
    const interval = setInterval(() => handleFetchServer(), 10000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  const handleCreateServerSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    axios
      .post('/server', { serverName })
      .then(() => {
        setIsCreatingServer(false);
        setServerName('');
        getServer().then((servers) => {
          setServers(servers);
        });
        toast.success('Serveur créé avec succès !');
      })
      .catch(({ response }) => {
        if (response.status === 401) {
          toast.error('Champ autorisation manquant');
        }
        if (response.status === 500) {
          toast.error('Erreur Serveur');
        }
      });
  };

  const handleLogout = () => {
    localStorage.removeItem('user');
    setUser(undefined);
    toast.success('Déconnecté avec succès !');
  };

  const handleJoinServer = (serverID: number) => {
    axios
      .post('/server/join', { serverID }, { })
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleLeaveServer = (serverID: number) => {
    axios
      .post('/server/leave', { serverID }, { })
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <div className="sidebar">
      <div className="sidebar-header">
        <h2>{t('servers')}</h2>
        <button type="button" onClick={() => setIsCreatingServer(true)}>{t('create_server')}</button>
      </div>
      <div className="sidebar-adder">
        {isCreatingServer && (
          <ModalForm
            handleSubmit={handleCreateServerSubmit}
            handleClose={() => setIsCreatingServer(false)}
          >
            <input
              type="text"
              value={serverName}
              onChange={(event) => setServerName(event.target.value)}
              placeholder="Nom du serveur"
              autoFocus
            />
          </ModalForm>
        )}
      </div>
      <div className="sidebar-items">
        {servers.map((server) => (
          <SidebarItem key={server.serverID} name={server.serverName} image="https://via.placeholder.com/60" handleClick={() => setServerId(server.serverID)} isSelected={serverId === server.serverID} />
        ))}
      </div>
      <div className="sidebar-footer">
        <button onClick={handleLogout}>{t('logout')}</button>
      </div>
    </div>
  );
}

export function ChannelSidebar() {
  const [channels, setChannels] = useState<Channel[]>([]);
  const [channelName, setChannelName] = useState<string>();
  const [isCreatingChannel, setIsCreatingChannel] = useState(false);

  const { user, serverId, channelId, setChannelId, setSocket } = useContext(UserContext);

  const handleCreateChannelSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!serverId) return;
    axios
      .post('/channel', { serverID: serverId, channelName })
      .then(() => {
        setIsCreatingChannel(false);
        setChannelName('');
        getChannel(serverId).then((channels) => {
          setChannels(channels);
        });
        toast.success('Serveur créé avec succès !');
      })
      .catch(({ response }) => {
        if (response.status === 401) {
          toast.error('Champ autorisation manquant');
        }
        if (response.status === 500) {
          toast.error('Erreur Serveur');
        }
      });
  };

  const handleClick = async (channelId: number) => {
    if (!user || !user.userID) return;
    setChannelId(channelId)
  };

  useEffect(() => {
    if (!serverId) return;
    const handleFetchChannels = () => getChannel(serverId).then((channels) => {
      setChannels(channels);
    });
    handleFetchChannels();
    const interval = setInterval(() => handleFetchChannels(), 10000);
    return () => {
      clearInterval(interval);
    };
  }, [serverId]);

  if (!serverId) return <></>;

  return (
    <div className="sidebar">
      <div className="sidebar-header">
        <h2>{t('channels')}</h2>
        <button onClick={() => setIsCreatingChannel(true)}>{t('create_channel')}</button>
      </div>
      <div className="sidebar-adder">
        {isCreatingChannel && (
        <ModalForm handleSubmit={handleCreateChannelSubmit} handleClose={() => setIsCreatingChannel(false)}>
          <input
            type="text"
            value={channelName}
            onChange={(event) => setChannelName(event.target.value)}
            placeholder="Nom du channel"
            autoFocus
          />
        </ModalForm>
        )}

      </div>
      <div className="sidebar-items">
        {channels.map((channel) => (
          <SidebarItem key={channel.channelID} name={channel.channelName} handleClick={() => { handleClick(channel.channelID); }} isSelected={channelId === channel.channelID} />
        ))}
      </div>
    </div>
  );
}
