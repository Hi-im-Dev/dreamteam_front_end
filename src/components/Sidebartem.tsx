import { Channel, Server } from '../services/Servers';

type Props = {
  name: string;
  image?: string;
  handleClick: () => void;
  isSelected: boolean
};

function ServerItem({
  name, image, handleClick, isSelected,
}: Props) {
  return (
    <div className="sidebar-item" style={{ backgroundColor: isSelected ? 'red' : '#393c43' }} onClick={handleClick}>
      {image && <img className="sidebar-image" src={image} alt={name} />}
      <div className="sidebar-text">{`${image ? '' : '#'}${name}`}</div>
    </div>
  );
}

export default ServerItem;
