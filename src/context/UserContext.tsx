import React, { createContext, useEffect, useState } from 'react';
import axios from '../services/Axios';

export type User = {
  userID?: number;
  username?: string;
  email?: string;
  accessToken?: string;
  refreshToken?: string;
};

type ContextType = {
  user?: User;
  setUser: React.Dispatch<React.SetStateAction<User | undefined>>;
  serverId?: number;
  setServerId: React.Dispatch<React.SetStateAction<number | undefined>>;
  channelId?: number,
  setChannelId: React.Dispatch<React.SetStateAction<number | undefined>>;
  socket?: WebSocket,
  setSocket: React.Dispatch<React.SetStateAction<WebSocket | undefined>>;
};

export const UserContext = createContext<ContextType>({
  user: undefined,
  setUser: () => {},
  serverId: undefined,
  setServerId: () => {},
  channelId: undefined,
  setChannelId: () => {},
  socket: undefined,
  setSocket: () => {},
});

export function UserProvider({ children }: { children: React.ReactNode }) {
  const [user, setUser] = useState<User>();
  const [serverId, setServerId] = useState<number>();
  const [channelId, setChannelId] = useState<number>();
  const [socket, setSocket] = useState<WebSocket>();


  useEffect(() => {
    const storedUser = localStorage.getItem('user');
    if (storedUser) {
      setUser(JSON.parse(storedUser));
    } else {
      setUser(undefined);
    }
  }, []);

  useEffect(() => {
    if (user) {
      localStorage.setItem('user', JSON.stringify(user));
      axios.defaults.headers.common.Authorization = user.accessToken;
    }
  }, [user]);

  return (
    <UserContext.Provider value={{
      user, setUser, serverId, setServerId, channelId, setChannelId, socket, setSocket
    }}
    >
      {children}
    </UserContext.Provider>
  );
}
