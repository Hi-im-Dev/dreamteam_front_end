import i18next from 'i18next';
import React from 'react';
import ReactDOM from 'react-dom';
import { Toaster } from 'react-hot-toast';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import { UserProvider } from './context/UserContext';

i18next.init({
  lng: 'fr',
  fallbackLng: 'fr',
  debug: true,
  resources: {
    fr: {
      translation: {
        'login': 'Connexion',
        'email': 'Email',
        'password': 'Mot de passe',
        'need_account': 'Besoin d\'un compte ?',
        'register': 'S\'inscrire',
        'username': 'Nom d\'utilisateur',
        'confirm_password': 'Confirmation mot de passe',
        'continue': 'Continuer',
        'already_account': 'Tu as déja un compte ?',
        'create_account': 'Créer un compte',
        'welcome': 'Accueil',
        'create': 'Créer',
        'cancel': 'Annuler',
        'servers': 'Serveurs',
        'create_server': 'Créer un serveur',
        'logout': 'Déconnexion',
        'channels': 'Canaux',
        'create_channel': 'Créer un channel',
      }
    },
    en: {
      translation: {
        'login': 'Login',
        'email': 'Email',
        'password': 'Password',
        'need_account': 'Need an account ?',
        'register': 'Register',
        'username': 'Username',
        'confirm_password': 'Confirm password',
        'continue': 'Continue',
        'already_account': 'Already have an account ?',
        'create_account': 'Create an account',
        'welcome': 'Welcome',
        'create': 'Create',
        'cancel': 'Cancel',
        'servers': 'Servers',
        'create_server': 'Create a server',
        'logout': 'Logout',
        'channels': 'Channels',
        'create_channel': 'Create a channel',
      }
    }
  }
});

ReactDOM.render(
  <UserProvider>
    <BrowserRouter>
      <App />
    </BrowserRouter>
    <Toaster
      toastOptions={{
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      }}
    />
  </UserProvider>,
  document.getElementById('root'),
);
