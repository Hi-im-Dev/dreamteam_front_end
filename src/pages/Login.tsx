import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import toast from 'react-hot-toast';
import { UserContext } from '../context/UserContext';
import axios from '../services/Axios';
import './Login.css';
import { t } from 'i18next';

function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { setUser } = useContext(UserContext);

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    axios
      .post('/profile/login', { email, password })
      .then(({ data, status }) => {
        console.log(JSON.stringify(data, null, 2));
        if (status === 200) {
          setUser(data.data);
        }
      })
      .catch(({ response }) => {
        console.log(response.status);
        if (response.status === 400) {
          toast.error('Champs Manquants');
        }
        if (response.status === 401) {
          toast.error('Email ou mot de passe incorect');
        }
      });
  };

  return (
    <div className="container">
      <div className="login-container dark">
        <h1>{t('login')}</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="email">{t('email')}</label>
            <input type="text" id="email" value={email} onChange={(e) => setEmail(e.target.value)} />
          </div>
          <div className="form-group">
            <label htmlFor="password">{t('password')}</label>
            <input type="password" id="password" value={password} onChange={(e) => setPassword(e.target.value)} />
          </div>
          <button type="submit">{t('login')}</button>
          <span className="line-register">
            {t('need_account')}
            {' '}
            <Link to="/register" className="link-register">
              {t('register')}
            </Link>
          </span>
        </form>
      </div>
    </div>
    // <div className="login">
    //   <h2>Login</h2>
    //   <form onSubmit={handleSubmit}>
    //     <div className="form-control">
    //       <label htmlFor="email">Email:</label>
    //       <input
    //         type="text"
    //         id="email"
    //         placeholder="Enter Email"
    //         value={email}
    //         onChange={(e) => setEmail(e.target.value)}
    //       />
    //     </div>
    //     <div className="form-control">
    //       <label htmlFor="password">Password:</label>
    //       <input
    //         type="password"
    //         id="password"
    //         placeholder="Enter password"
    //         value={password}
    //         onChange={(e) => setPassword(e.target.value)}
    //       />
    //     </div>
    //     <button type="submit" className="btn btn-primary">
    //       Login
    //     </button>
    //     <Link to="/register" className="btn btn-secondary">
    //       Register
    //     </Link>
    //   </form>
    // </div>
  );
}

export default Login;
