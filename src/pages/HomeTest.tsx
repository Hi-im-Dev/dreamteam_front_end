import { t } from 'i18next';
import React from 'react';
import axios from '../services/Axios';
import './HomeTest.css';

function HomeTest() {
  const handleCreateServer = async () => {
    try {
      const response = await axios.post(
        '/server',
        { serverName: 'Excellium' },
        { },
      );
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  // SIDEBAR
  const handleJoinServer = async () => {
    try {
      const response = await axios.post(
        '/server/join',
        { serverID: 1 },
        { },
      );
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  // SIDEBAR
  const handleLeaveServer = async () => {
    try {
      const response = await axios.post(
        '/server/leave',
        { serverID: 1 },
        { },
      );
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  // SIDEBAR
  const handleCreateChannel = async () => {
    try {
      const response = await axios.post(
        '/channel',
        { serverID: 1, channelName: 'Le parking' },
        { },
      );
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  // SIDEBAR
  const handleJoinChannel = async () => {
    try {
      const response = await axios.post(
        '/channel/join',
        { channelID: 1 },
        { },
      );
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  // SIDEBAR
  const handleLeaveChannel = async () => {
    try {
      const response = await axios.post(
        '/channel/leave',
        { channelID: 1 },
        { },
      );
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="hometest">
      <h1>{t('welcome')}</h1>
      <p>Bienvenue sur notre application de test de serveur</p>
      <button type="button" onClick={handleCreateServer}>Créer un serveur</button>
      <button type="button" onClick={handleJoinServer}>Rejoindre un serveur</button>
      <button type="button" onClick={handleLeaveServer}>Quitter un serveur</button>
      <button type="button" onClick={handleCreateChannel}>Créer un channel</button>
      <button type="button" onClick={handleJoinChannel}>Rejoindre un channel</button>
      <button type="button" onClick={handleLeaveChannel}>Quitter un channel</button>
    </div>
  );
}

export default HomeTest;
