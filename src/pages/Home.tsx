import React, { useContext, useEffect } from 'react';
import toast from 'react-hot-toast';
import Chat from '../components/Chat';
import { ServerSidebar, ChannelSidebar } from '../components/Sidebar';
import { UserContext } from '../context/UserContext';
import { connectWebSocket } from '../services/Socket';
import './Home.css';

function Home() {
  const { user, setSocket } = useContext(UserContext);
  
  useEffect(() => {
    if (!user || !user.userID) return
    connectWebSocket(user.userID).then((ws) => setSocket(ws))
    toast.success('Connecté avec succès !');
  }, []);

  if (!user || !user.username) return <></>;
  return (
    <div className="home">
      <ServerSidebar />
      <ChannelSidebar />
      <Chat />
    </div>
  );
}

export default Home;
