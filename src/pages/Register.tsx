import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import toast from 'react-hot-toast';
import { AxiosError } from 'axios';
import axios from '../services/Axios';
import './Register.css';
import { t } from 'i18next';

function Register() {
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    axios
      .post('/profile/register', { username, email, password })
      .then(() => {
        console.log('User registered successfully');
        toast.success('Inscription réussie !');
      })
      .catch((error: AxiosError) => {
        if (error.status === 400) {
          toast.error('Champs Manquants');
        }
        if (error.status === 409) {
          toast.error('Email déjà utilisé');
        }
        if (error.status === 503) {
          toast.error("Problèmes lors de la création de l'utilisateur");
        }
      });
  };

  return (
    <div className="container">
      <div className="register-container dark">
        <h1>{t('create_account')}</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="email">{t('email')}</label>
            <input type="text" id="email" value={email} onChange={(e) => setEmail(e.target.value)} />
          </div>
          <div className="form-group">
            <label htmlFor="username">{t('username')}</label>
            <input type="text" id="username" value={username} onChange={(e) => setUsername(e.target.value)} />
          </div>
          <div className="form-group">
            <label htmlFor="password">{t('password')}</label>
            <input type="password" id="password" value={password} onChange={(e) => setPassword(e.target.value)} />
          </div>
          <div className="form-group">
            <label htmlFor="password">{t('confirm_password')}</label>
            <input type="password" id="passwordcheck" value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} />
          </div>
          <button type="submit">{t('continue')}</button>
          <Link to="/login" className="link-register">
            {t('already_account')}
          </Link>
        </form>
      </div>
    </div>

  // <div className="register">
  //   <h2>Register</h2>
  //   <form onSubmit={handleSubmit}>
  //     <div className="form-control">
  //       <label htmlFor="email">Email:</label>
  //       <input
  //         type="text"
  //         id="email"
  //         placeholder="Enter email"
  //         value={email}
  //         onChange={(e) => setEmail(e.target.value)}
  //       />
  //     </div>
  //     <div className="form-control">
  //       <label htmlFor="username">Username:</label>
  //       <input
  //         type="text"
  //         id="username"
  //         placeholder="Enter username"
  //         value={username}
  //         onChange={(e) => setUsername(e.target.value)}
  //       />
  //     </div>
  //     <div className="form-control">
  //       <label htmlFor="password">Password:</label>
  //       <input
  //         type="password"
  //         id="password"
  //         placeholder="Enter password"
  //         value={password}
  //         onChange={(e) => setPassword(e.target.value)}
  //       />
  //     </div>
  //     <div className="form-control">
  //       <label htmlFor="confirmPassword">Confirm Password:</label>
  //       <input
  //         type="password"
  //         id="confirmPassword"
  //         placeholder="Confirm password"
  //         value={confirmPassword}
  //         onChange={(e) => setConfirmPassword(e.target.value)}
  //       />
  //     </div>
  //     <button type="submit" className="btn btn-primary">
  //       Register
  //     </button>
  //     <Link to="/login" className="btn btn-secondary">
  //       Login
  //     </Link>
  //   </form>
  // </div>
  );
}

export default Register;
