import Axios, { AxiosRequestConfig } from 'axios';

export const config: AxiosRequestConfig = {
  baseURL: `${import.meta.env.VITE_BACKEND_IP}${import.meta.env.VITE_BACKEND_PORT ? `:${import.meta.env.VITE_BACKEND_PORT}` : ''}`,
  timeout: 15000,
};

console.log(`running in ${import.meta.env.DEV ? 'debug' : 'release'} mode`);
console.log('config: ', config);
const axios = Axios.create(config);

export default axios;
