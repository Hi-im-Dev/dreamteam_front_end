import axios from './Axios';

export type Server = {
  serverID: number
  serverName: string
};

export type Channel = {
  channelID: number
  channelName: string
};

export const getServer = () => axios
  .get<Server[]>('/server')
  .then((response) => response.data);

export const getChannel = (serverId: number) => axios
  .get<Channel[]>(
  `/channel/${serverId}`,
)
  .then((response) => response.data);
