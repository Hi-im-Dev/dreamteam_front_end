const socketUrl = import.meta.env.VITE_WS_URL ?? 'ws://localhost:8080';

export async function connectWebSocket(userId: number) {
  const socket = new WebSocket(`${socketUrl}?userId=${userId}`);

  // Écouter l'événement "open" pour savoir quand la connexion WebSocket est établie
  socket.addEventListener('open', (event) => {
    console.log(`WebSocket connected with user ID ${userId}`);
  });

  return socket;
}
