import React, { useContext } from 'react';
import {
  Switch, Route, Redirect, RouteProps, RouteComponentProps,
} from 'react-router-dom';
import HomeTest from './pages/HomeTest';
import Register from './pages/Register';
import Login from './pages/Login';
import Home from './pages/Home';
import { User, UserContext } from './context/UserContext';
import './App.css';

interface PrivateRouteProps extends RouteProps {
  render: (props: RouteComponentProps<{ [x: string]: string | undefined; }>) => JSX.Element;
  user: User | undefined;
}

function PrivateRoute({
  render: RenderComponent,
  user,
  path,
}: PrivateRouteProps) {
  return (
    <Route
      path={path}
      render={(props) => (user ? <RenderComponent {...props} /> : <Redirect to="/login" />)}
    />
  );
}

function App() {
  const { user } = useContext(UserContext);

  function LogoutRoute({
    render: RenderComponent,
    ...rest
  }: PrivateRouteProps) {
    return (
      <Route
        {...rest}
        render={(props) => (user ? (<Redirect to="/" />) : (RenderComponent && <RenderComponent {...props} />))}
      />
    );
  }

  return (
    <div>
      <Switch>
        <LogoutRoute user={user} path="/register" render={Register} />
        <LogoutRoute user={user} path="/login" render={Login} />
        <PrivateRoute user={user} path="/" render={Home} />
        <PrivateRoute user={user} path="/test" render={HomeTest} />
      </Switch>
    </div>
  );
}

export default App;
