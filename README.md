# DreamTeam Front-end

This is the front-end repository for the DreamTeam application.

## Requirements

Node.js (v14 or later)

npm or yarn
## Installation

- Clone the repository

```bash
git clone https://github.com/yourusername/dreamteam_front_end.git
```

- Install dependencies

```bash
cd dreamteam_front_end
npm install
```
## Usage
### Development

Start the development server with hot reloading:

```
npm run start
```
This will start the development server on http://localhost:8081.

### Production

Build the project for production:

```
npm run build
```
This will compile the TypeScript files and create a production-ready build in the dist directory.

### Preview

Preview the production build:

```
npm run preview
```
This will start a server that serves the production build on http://localhost:5000.

### Linting

Run the linter:
```
npm run lint
```
This will run the ESLint linter on the src directory. It uses the Airbnb style guide with TypeScript extensions.